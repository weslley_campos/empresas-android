package com.github.weslleystos.features.empresa_detail.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.github.weslleystos.R
import com.github.weslleystos.databinding.ActivityEnterpriseDetailBinding
import com.github.weslleystos.features.shared.entities.Enterprise

class EnterpriseDetailActivity : AppCompatActivity() {
    companion object {
        lateinit var enterprise: Enterprise
        fun newInstance(context: Context, enterprise: Enterprise): Intent {
            this.enterprise = enterprise
            return Intent(context, EnterpriseDetailActivity::class.java)
        }
    }

    private lateinit var binding: ActivityEnterpriseDetailBinding
    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEnterpriseDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        this.apply {
            window.statusBarColor = ContextCompat.getColor(this, R.color.black_pink)
        }
        toolbar = binding.toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.run {
            title = enterprise.name
            setDisplayHomeAsUpEnabled(true)
        }
        toolbar.setNavigationOnClickListener { onBackPressed() }

        binding.enterprise = enterprise
    }
}