package com.github.weslleystos.features.login.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.github.weslleystos.R
import com.github.weslleystos.databinding.FragmentLoginBinding
import com.github.weslleystos.features.login.viewmodels.AccountViewModel
import com.github.weslleystos.features.shared.entities.ResponseStatus
import com.github.weslleystos.features.shared.entities.Result
import com.github.weslleystos.features.shared.ui.MainActivity
import com.github.weslleystos.features.shared.utils.hide
import com.github.weslleystos.features.shared.utils.show
import com.google.android.material.textfield.TextInputEditText

class LoginFragment : Fragment() {

    private val credentials = mutableMapOf<String, String>()
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!
    private val accountViewModel: AccountViewModel by viewModels()
    private lateinit var progressOverlay: FrameLayout
    private lateinit var inputEmail: TextInputEditText
    private lateinit var inputPassword: TextInputEditText
    private lateinit var credentialsError: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).window.statusBarColor =
            ContextCompat.getColor(requireContext(), R.color.beige_black)

        progressOverlay = binding.dialogProgress
        inputEmail = binding.email
        inputPassword = binding.password
        credentialsError = binding.credentialsError

        binding.btnLogin.setOnClickListener {
            progressOverlay.show()
            credentialsError.hide()
            credentials["email"] = inputEmail.text.toString()
            credentials["password"] = inputPassword.text.toString()
            accountViewModel.signIn(credentials)
        }
        accountViewModel.sessionLiveData.observe(viewLifecycleOwner, signInObserver)
    }

    private val signInObserver = Observer<Pair<ResponseStatus, Result<Any>?>> { pair ->
        progressOverlay.hide()
        when (pair.first) {
            ResponseStatus.SUCCESS -> {
                findNavController().navigate(R.id.action_login_to_enterprises)
            }
            ResponseStatus.FAILED -> {
                credentialsError.show()
                inputEmail.error = getString(R.string.invalid_credentials)
            }
            ResponseStatus.ERROR -> {
                credentialsError.show()
                credentialsError.error = getString(R.string.network_error)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}