package com.github.weslleystos.features.enterprises_list.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.github.weslleystos.databinding.EnterpriseRowBinding
import com.github.weslleystos.features.shared.entities.Enterprise

class EnterprisesAdapter(
    private val itemClickListener: (Enterprise) -> Unit
) : RecyclerView.Adapter<EnterprisesAdapter.EnterpriseVH>() {
    private var enterprises: MutableList<Enterprise> = mutableListOf()

    fun setEnterprises(enterprises: List<Enterprise>) {
        this.enterprises.clear()
        this.enterprises.addAll(enterprises)
        notifyDataSetChanged()
    }

    fun clearEnterprises() {
        this.enterprises.clear()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EnterpriseVH {
        return EnterpriseVH(
            EnterpriseRowBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: EnterpriseVH, position: Int) {
        holder.bind(enterprises[position])
    }

    override fun getItemCount() = enterprises.size

    inner class EnterpriseVH(private val binding: EnterpriseRowBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(enterprise: Enterprise) {
            binding.enterprise = enterprise
            binding.enterpriseCard.setOnClickListener { itemClickListener(enterprise) }
        }
    }
}