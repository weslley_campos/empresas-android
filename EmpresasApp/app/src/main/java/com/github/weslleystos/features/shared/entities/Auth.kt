package com.github.weslleystos.features.shared.entities

data class Auth(val token: String, val client: String, val uid: String)