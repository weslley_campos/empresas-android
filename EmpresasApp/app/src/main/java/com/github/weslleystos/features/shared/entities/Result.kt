package com.github.weslleystos.features.shared.entities

data class Result<T>(private val data: T)