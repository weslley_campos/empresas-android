package com.github.weslleystos.features.shared.utils

import com.github.weslleystos.App
import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        App.auth?.let {
            val request = chain.request().newBuilder()
                .addHeader("Content-Type", "application/json")
                .addHeader("Accept", "application/json")
                .addHeader("access-token", it.token)
                .addHeader("client", it.client)
                .addHeader("uid", it.uid)
                .build()
            return chain.proceed(request)
        }
        return chain.proceed(chain.request())
    }
}