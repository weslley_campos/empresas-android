package com.github.weslleystos.features.login.services

import com.github.weslleystos.features.login.repositories.remote.IAccountRepository
import com.github.weslleystos.features.shared.services.RetrofitService

object AccountService {
    private val accountRepository = RetrofitService.create<IAccountRepository>()

    suspend fun signIn(credentials: Map<String, String>) = accountRepository.signIn(credentials)
}
