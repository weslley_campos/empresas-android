package com.github.weslleystos.features.enterprises_list.ui

import android.content.Context
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import android.widget.ViewFlipper
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.weslleystos.R
import com.github.weslleystos.databinding.FragmentListEnterprisesBinding
import com.github.weslleystos.features.empresa_detail.ui.EnterpriseDetailActivity
import com.github.weslleystos.features.enterprises_list.viewmodel.EnterpriseViewModel
import com.github.weslleystos.features.shared.entities.Enterprise
import com.github.weslleystos.features.shared.entities.ResponseStatus
import com.github.weslleystos.features.shared.ui.MainActivity

class EnterprisesListFragment : Fragment() {

    companion object {
        private const val SEARCH_LAYOUT = 0
        private const val NOT_FOUND_LAYOUT = 1
        private const val LIST_LAYOUT = 2
        private const val LOADING_LAYOUT = 3
    }

    private var _binding: FragmentListEnterprisesBinding? = null
    private val binding get() = _binding!!
    private lateinit var flipper: ViewFlipper
    private lateinit var enterpriseAdapter: EnterprisesAdapter
    private val enterpriseViewModel: EnterpriseViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)
        _binding = FragmentListEnterprisesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).apply {
            setSupportActionBar(binding.toolbar)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            window.statusBarColor = ContextCompat.getColor(requireContext(), R.color.black_pink)
        }

        binding.toolbar.inflateMenu(R.menu.menu_home)
        flipper = binding.viewFlipper

        enterpriseAdapter = EnterprisesAdapter(onItemClickListener)
        val recyclerView = binding.recyclerEnterprises
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = enterpriseAdapter
        }

        if (enterpriseAdapter.itemCount > 0) {
            flipper.displayedChild = LIST_LAYOUT
        }

        enterpriseViewModel.enterprisesLiveData.observe(viewLifecycleOwner, enterpriseObserver)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_home, menu)

        val searchItem: MenuItem = menu.findItem(R.id.action_search)
        val searchView = searchItem.actionView as SearchView

        searchItem.setOnActionExpandListener(searchExpandListener)
        searchView.setOnQueryTextListener(searchListener)
    }

    private val enterpriseObserver = Observer<Pair<ResponseStatus, List<Enterprise>?>> { pair ->
        when (pair.first) {
            ResponseStatus.SUCCESS -> {
                pair.second?.let {
                    if (it.isEmpty()) {
                        flipper.displayedChild = NOT_FOUND_LAYOUT
                        enterpriseAdapter.clearEnterprises()
                    } else {
                        flipper.displayedChild = LIST_LAYOUT
                        enterpriseAdapter.setEnterprises(it)
                    }
                }
            }
            ResponseStatus.FAILED -> {
                findNavController().navigate(R.id.action_detail_to_login)
            }
            ResponseStatus.ERROR -> {
                Toast.makeText(context, getString(R.string.network_error), Toast.LENGTH_SHORT)
                    .show()
                flipper.displayedChild = SEARCH_LAYOUT
            }
        }
    }

    private val searchListener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            if (!query.isNullOrEmpty()) {
                enterpriseViewModel.findByName(query)
                flipper.displayedChild = LOADING_LAYOUT
            }
            hiddenKeyboard()
            return true
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            return false
        }
    }

    private fun hiddenKeyboard() {
        val manager = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        manager.hideSoftInputFromWindow(
            activity?.currentFocus?.windowToken, 0
        )
    }

    private val searchExpandListener = object : MenuItem.OnActionExpandListener {
        override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
            return true
        }

        override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
            if (enterpriseAdapter.itemCount == 0) {
                flipper.displayedChild = SEARCH_LAYOUT
            }
            return true
        }
    }

    private val onItemClickListener: (Enterprise) -> Unit = {
        val intent = EnterpriseDetailActivity.newInstance(requireContext(), it)
        startActivity(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_search -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}