package com.github.weslleystos.features.shared.utils

//sealed class RequestResult {
//    companion object {
//        fun <T> success(data: T): RequestResult = Success(data)
//        fun <T> error(code: Int, data: T?): RequestResult = Error(code, data)
//    }
//    data class Success<T>(val data: T) : RequestResult()
//    data class Error<T>(val code: Int, val data: T?) : RequestResult()
//}

data class RequestResult<out T>(val status: RequestStatus, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T?): RequestResult<T> =
            RequestResult(status = RequestStatus.SUCCESS, data = data, message = null)

        fun <T> failed(message: String?): RequestResult<T> =
            RequestResult(status = RequestStatus.FAILED, data = null, message = message)

        fun <T> error(data: T?, message: String?): RequestResult<T> =
            RequestResult(status = RequestStatus.ERROR, data = data, message = message)

        fun <T> loading(): RequestResult<T> =
            RequestResult(status = RequestStatus.LOADING, data = null, message = null)
    }
}