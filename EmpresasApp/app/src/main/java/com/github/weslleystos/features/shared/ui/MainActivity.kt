package com.github.weslleystos.features.shared.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.github.weslleystos.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}