package com.github.weslleystos.features.enterprises_list.services

import com.github.weslleystos.features.enterprises_list.repositories.remote.IEnterpriseRepository
import com.github.weslleystos.features.shared.services.RetrofitService

object EnterpriseService {
    private val enterpriseRepository = RetrofitService.create<IEnterpriseRepository>()

    suspend fun findByName(enterprise: String) = enterpriseRepository.findByName(enterprise)
}