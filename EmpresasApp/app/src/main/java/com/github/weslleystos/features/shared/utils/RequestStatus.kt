package com.github.weslleystos.features.shared.utils

enum class RequestStatus {
    SUCCESS,
    ERROR,
    FAILED,
    LOADING
}