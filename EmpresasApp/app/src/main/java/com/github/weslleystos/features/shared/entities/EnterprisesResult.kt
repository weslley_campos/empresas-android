package com.github.weslleystos.features.shared.entities

data class EnterprisesResult(val enterprises: List<Enterprise>)