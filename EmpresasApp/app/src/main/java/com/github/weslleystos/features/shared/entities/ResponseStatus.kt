package com.github.weslleystos.features.shared.entities

enum class ResponseStatus {
    SUCCESS,
    FAILED,
    ERROR,
}