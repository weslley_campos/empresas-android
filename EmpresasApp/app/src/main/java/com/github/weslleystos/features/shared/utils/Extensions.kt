package com.github.weslleystos.features.shared.utils

import android.view.View
import android.widget.FrameLayout
import android.widget.TextView


fun FrameLayout.show() {
    this.visibility = View.VISIBLE
}

fun FrameLayout.hide() {
    this.visibility = View.GONE
}

fun TextView.show() {
    this.visibility = View.VISIBLE
}

fun TextView.hide() {
    this.visibility = View.GONE
}