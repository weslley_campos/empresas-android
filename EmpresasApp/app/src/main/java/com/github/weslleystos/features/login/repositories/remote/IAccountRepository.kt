package com.github.weslleystos.features.login.repositories.remote

import com.github.weslleystos.features.shared.entities.Result
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface IAccountRepository {
    @POST("users/auth/sign_in")
    suspend fun signIn(@Body credentials: Map<String, String>): Response<Result<Any>>
}