package com.github.weslleystos.features.login.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.github.weslleystos.App
import com.github.weslleystos.features.login.services.AccountService
import com.github.weslleystos.features.shared.entities.Auth
import com.github.weslleystos.features.shared.entities.ResponseStatus
import com.github.weslleystos.features.shared.entities.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.Headers

class AccountViewModel : ViewModel() {

    val sessionLiveData: MutableLiveData<Pair<ResponseStatus, Result<Any>?>> = MutableLiveData()

    fun signIn(credentials: Map<String, String>) = viewModelScope.launch(Dispatchers.IO) {
        try {
            val response = AccountService.signIn(credentials)
            withContext(Dispatchers.Main) {
                when {
                    response.isSuccessful -> {
                        setHeadersToAuth(response.headers())
                        sessionLiveData.value = Pair(ResponseStatus.SUCCESS, null)
                    }
                    else -> sessionLiveData.value = Pair(ResponseStatus.FAILED, null)
                }
            }
        } catch (e: Exception) {
            print("")
            withContext(Dispatchers.Main) {
                sessionLiveData.value = Pair(ResponseStatus.ERROR, null)
            }
        }
    }

    private fun setHeadersToAuth(headers: Headers) {
        val uid = headers["uid"]!!
        val token = headers["access-token"]!!
        val client = headers["client"]!!

        App.auth = Auth(token, client, uid)
    }
}