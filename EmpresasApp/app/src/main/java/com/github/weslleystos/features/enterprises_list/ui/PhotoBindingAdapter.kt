package com.github.weslleystos.features.enterprises_list.ui

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.github.weslleystos.R
import com.github.weslleystos.features.shared.services.RetrofitService


object PhotoBindingAdapter {
    @JvmStatic
    @BindingAdapter("photo")
    fun fetchPhoto(image: ImageView, photo: String?) {
        photo?.let { url ->
            Glide.with(image.context)
                .load(RetrofitService.SERVER + url)
                .transition(withCrossFade())
                .placeholder(R.drawable.placeholder)
                .into(image)
        }
    }
}