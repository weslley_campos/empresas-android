package com.github.weslleystos.features.shared.entities

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EnterpriseType(
    val id: Int,

    @SerializedName("enterprise_type_name")
    val name: String
) : Parcelable