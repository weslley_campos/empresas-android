package com.github.weslleystos.features.enterprises_list.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.github.weslleystos.features.enterprises_list.services.EnterpriseService
import com.github.weslleystos.features.shared.entities.Enterprise
import com.github.weslleystos.features.shared.entities.ResponseStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class EnterpriseViewModel : ViewModel() {

    val enterprisesLiveData: MutableLiveData<Pair<ResponseStatus, List<Enterprise>?>> =
        MutableLiveData()

    fun findByName(enterprise: String) = viewModelScope.launch(Dispatchers.IO) {
        try {
            val response = EnterpriseService.findByName(enterprise)
            withContext(Dispatchers.Main) {
                when {
                    response.isSuccessful -> {
                        enterprisesLiveData.value =
                            Pair(ResponseStatus.SUCCESS, response.body()?.enterprises)
                    }
                    else -> enterprisesLiveData.value = Pair(ResponseStatus.FAILED, null)
                }
            }
        } catch (e: Exception) {
            withContext(Dispatchers.Main) {
                enterprisesLiveData.value = Pair(ResponseStatus.ERROR, null)
            }
        }
    }
}