package com.github.weslleystos.features.shared.entities

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Enterprise(
    val id: Int,

    @SerializedName("enterprise_name")
    val name: String,

    val country: String,

    val description: String,

    val photo: String,

    @SerializedName("enterprise_type")
    val type: EnterpriseType
) : Parcelable