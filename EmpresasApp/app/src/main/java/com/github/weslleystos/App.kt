package com.github.weslleystos

import android.app.Application
import com.github.weslleystos.features.shared.entities.Auth

class App : Application() {
    companion object {
        var auth: Auth? = null
    }
}