package com.github.weslleystos.features.enterprises_list.repositories.remote

import com.github.weslleystos.features.shared.entities.EnterprisesResult
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface IEnterpriseRepository {
    @GET("enterprises")
    suspend fun findByName(@Query("name") enterprise: String): Response<EnterprisesResult>
}
